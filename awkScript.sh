#!/bin/bash
clear
printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n"
printf "# The following items are melons:\n"
awk '/melon/ {print $0}' mysampledata.txt
printf "\n# The following items have limited stock:\n" 
awk '{ if($3 <= 9) {print}}' mysampledata.txt
printf "\n# The following people sell oranges:\n"
awk '/oranges/ {print $1}' mysampledata.txt
printf "\n# The following orange growers have stock between 6 and 15:\n"
awk '/oranges/ { if(($3 >= 6) && ($3 <= 15)) {print}}' mysampledata.txt
printf "\n# The total number of oranges was "
awk '/oranges/ { sum+=$3 } END {print sum}' mysampledata.txt

printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"

 
